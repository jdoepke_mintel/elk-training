## Elasticsearch
## Kibana
---
### Follow along

http://bitbucket.org/jdoepke/elk-training
---
### What is Elasticsearch?

- Search engine based on Lucene.
- Provides a distributed, multitenant-capable full-text search engine with an HTTP web interface and schema-free JSON documents.
- Developed in Java and is released as open source under the terms of the Apache License.
+++
### You've just described Solr
##### We're doomed
+++
### Terminology Difference

Solr | Elasticsearch
-----|--------------
collection | index
+++
### Logstash
+++
### Kibana
---
### ELK Stack
![Logo](https://www.stratalux.com/wp-content/uploads/2016/06/Will-Migrating-to-the-Cloud-Save-Money-5.png)
+++
### Beats
---
### B ELK?
![Logo](https://www.elastic.co/assets/blt08fb2e862c57956e/elastic-elk-b.png)
---
#### Elastic Stack - Brought to you by...
![Logo](http://www.kluren.com/images/technologies/elasticsearch.png)

https://www.elastic.co/

(You should go here for docs.)
